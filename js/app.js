$(document).foundation()

$(document).ready(function(){
	$('.ddwn').click(function(e){
		e.preventDefault();
		$(this).next().slideToggle(200);
		$(this).toggleClass("active");
	})

	$('.dash-logged').click(function(e){
		e.preventDefault();
		$('.logged-in-dropdown').fadeToggle(300);
	})

	$('#nav-icon1').click(function(){
		$(this).toggleClass('open');
		$('.mobile-menu').slideToggle(200);
	});

	$(function() {
		$('.item-high').matchHeight();
	});

	$(function() {
		$('.dashboard-height').matchHeight();
	});

	$(function() {
		$('.features-high').matchHeight();
	});

	$(function() {
		$('.dash-high').matchHeight();
	});

	$(function() {
		$('.bt-height').matchHeight();
	});

	$(function() {
		$('.footer-height').matchHeight();
	});

})

$(document).ready(function(){
	$('select').niceSelect();
})